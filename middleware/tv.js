'use strict';
// Render tv page. The page has client side js for randomly playing a file from the files list
var utilsLib = require('../lib/utils');

module.exports = function (req, res) {
	var slug = req.params && req.params.slug,
		channel = slug && {
			title: utilsLib.toTitleCase(slug),
			path: slug + '/'
		};

	if(channel) {
		res.render('tv', {
			title: config.title
		});
	} else {
		res.render('error', {
			message: 'No tv found for ' + slug
		});
	}
}
