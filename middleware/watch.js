'use strict';
// Render a watch page which will have the video file in query params

var path = require('path'),
	fs = require('fs'),
	config = JSON.parse(fs.readFileSync('config.json')),
	utilsLib = require('../lib/utils');

module.exports = function (req, res) {
	var videoFile = media + '/' + req.query['file'];
	if(videoFile) {
		res.render('watch', {
			videoSource: '/video?file=' + videoFile,
			// parse the name for this video file
			videoTitle: utilsLib.parseTitleFromFileName(videoFile),
			title: config.title
		});
	} else {
		// No file provided in the query params
		res.render('error', { message: 'Video file not provided in the query params!' });
	}
}
