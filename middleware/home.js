'use strict';
var fs = require('fs'),
	utilsLib = require('../lib/utils');

module.exports = function (req, res) {
	var channels = [];
	// Get available channels by reading the media dir
	fs.readdir(fs.readlinkSync(media), function (err, files) {
		if(err) {
			res.render('error', {
				message: 'Something went wrong while reading channels from media dir'
			});
		}
		files.forEach(function (dir) {
			if (dir[0] !== '.') {
				channels.push({
					slug: dir,
					title: utilsLib.toTitleCase(dir)
				});
			}
		});
		res.render('index', {
			title: config.title,
			channels: channels
		});
	});
}
