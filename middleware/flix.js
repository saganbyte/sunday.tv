'use strict';
// Load a list of files to chose and play
var fs = require('fs'),
	utilsLib = require('../lib/utils');

module.exports = function (req, res) {
	var slug = req.query && req.query.path,
		flix = slug && {
			title: utilsLib.toTitleCase(slug),
			path: slug + '/'
		}

	if(flix) {
		// Get a list of video files from this flix
		fs.readdir(media + '/' + slug, function (err, files) {
			if(err) {
				res.render('error', {
					message: 'Something went wrong while reading files for ' + slug
				});
			}
			res.render('flix', {
				flix: flix,
				files: files,
				title: config.title
			});
		});
	} else {
		res.render('error', {
			message: 'No flix found for ' + slug
		});
	}

}
