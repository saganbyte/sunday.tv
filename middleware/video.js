'use strict';
// Stream the requested video file
var fs = require('fs');

module.exports = function (req, res) {
	var videoFile = req.query['file'];
	console.log(videoFile);
	fs.stat(videoFile, function (err, stats) {
		if(err) {
			console.log(err);
		}

		var total = stats.size;
		if (req.headers['range']) {
			var range = req.headers.range,
				parts = range.replace(/bytes=/, '').split('-'),
				partialstart = parts[0],
				partialend = parts[1],
				start = parseInt(partialstart, 10),
				end = partialend ? parseInt(partialend, 10) : total - 1,
				chunksize = (end-start) + 1,
				file = fs.createReadStream(videoFile, {start: start, end: end});

			console.log('RANGE: ' + start + ' - ' + end + ' = ' + chunksize); //RANGE: 0 - 71956203 = 71956204

			res.writeHead(206, {
				'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
				'Accept-Ranges': 'bytes',
				'Content-Length': chunksize,
				'Content-Type': 'video/mp4'
			});
			file.pipe(res);
		} else {
			console.log('ALL: ' + total);
			res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'video/mp4' });
			fs.createReadStream(videoFile).pipe(res);
		}
	});

}
