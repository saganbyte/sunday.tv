'use strict';
// Render a watch page which will have the video file in query params

var path = require('path'),
	fs = require('fs'),
	config = JSON.parse(fs.readFileSync('config.json')),
	utilsLib = require('../lib/utils');

module.exports = function (req, res) {
	var imgFile = media + '/' + req.query['file'];
	if(imgFile) {
		res.sendFile(imgFile);
	} else {
		// No file provided in the query params
		res.render('error', { message: 'Video file not provided in the query params!' });
	}
}
