'use strict';
// Generate a list of media files (requested by client side js via XHR
var fs = require('fs'),
	readDirFiles = require('read-dir-files');

module.exports = function (req, res) {
	var channel = req.body && req.body.channel,
		mediaFiles = [];
	if (channel) {
		readDirFiles.list(media + '/' + channel, function (err, files) {
			// Remove anything that does not end with a mp4
			files = files.filter(function(el) {
				return (el.indexOf('.mp4') > 0);
			});
			res.json(files);
		});
	}
}
