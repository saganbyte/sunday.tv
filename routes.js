var express = require('express'),
	router = express.Router(),
	fs = require('fs'),
	path = require('path'),
	utilsLib = require('./lib/utils');
	config = JSON.parse(fs.readFileSync('config.json')),
	home = require('./middleware/home'),
	flix = require('./middleware/flix'),
	watch = require('./middleware/watch'),
	image = require('./middleware/image'),
	video = require('./middleware/video'),
	tv = require('./middleware/tv'),
	files = require('./middleware/files'),
	media = config.media;

/* GET home page. */
router.get('/', home);

/* GET list of videos select from. */
router.get('/flix', flix);

/* GET watch page. */
router.get('/watch', watch);

/* GET image page. */
router.get('/image', image);

/* GET video. */
router.get('/video', video);

/* GET tv */
router.get('/tv/:slug', tv);

/** POST channel/folder name and return all the video files from it */
router.post('/files', files);

module.exports = router;
