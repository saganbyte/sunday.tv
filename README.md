# Sunday TV (WIP)
-----------------

Create a television watching experience from your video files. Create a symbolic at `/tv` that points to your media directory (the pointer should not use a relative path such as `~`). The media directory should have directories that will be considered as channels. Each channel (directory) can have video files or directories with videos.

All video files must be mp4. Use HandbrakeCLI for encoding your video files with the command
```
HandBrakeCLI -i "filename.avi" -o "filename.mp4" -e x264 -q 20 -a 1 -E faac -B 128 -6 dpl2 -R Auto -D 0.0 -f mp4 -X 640 -m -x cabac=0:ref=2:me=umh:bframes=0:weightp=0:subme=6:8x8dct=0:trellis=0 --vb 600 --two-pass --turbo --optimize
```
