// Full screen button (for browsers that dont support the player's full screen button
(function() {
	var tv = document.getElementsByTagName('video')[0];
	function makeFS() {
		if (
			document.fullscreenEnabled ||
			document.webkitFullscreenEnabled ||
			document.mozFullScreenEnabled ||
			document.msFullscreenEnabled
		) {
			if (tv.requestFullscreen) {
				tv.requestFullscreen();
			} else if (tv.webkitRequestFullscreen) {
				tv.webkitRequestFullscreen();
			} else if (tv.mozRequestFullScreen) {
				tv.mozRequestFullScreen();
			} else if (tv.msRequestFullscreen) {
				tv.msRequestFullscreen();
			}
		}
	}
	function moveCurrentTime(param) {
		tv.currentTime += param;
	}
	$('#fs').on('click', makeFS);
	$('#for').on('click', moveCurrentTime.bind(null, 10));
	$('#rew').on('click', moveCurrentTime.bind(null, -10));
	$('#prev').on('click', moveCurrentTime.bind(null, -300));
	$('#next').on('click', moveCurrentTime.bind(null, 300));
}());
