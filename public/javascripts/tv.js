(function () {
	var tv = document.getElementsByTagName('video')[0],
		path = location.pathname,
		channel = path.slice(path.lastIndexOf('/') + 1),
		videoFiles,
		totalVideoFiles;

	// Add event listener to video player to load a random video
	// whenever the current video ends playing
	tv.addEventListener('ended', playRandomVideo, false);

	// Fetch all the video files for the current channel
	$.ajax({
		type: 'post',
		url: '/files',
		data: {
			channel: channel
		},
		success: function (files) {
			videoFiles = files;
			totalVideoFiles = files.length;
			playRandomVideo();
		},
		error: function () {
			console.log('something went wrong!');
		}
	});

	function playRandomVideo() {
		var video = videoFiles[Math.round(Math.random() * (totalVideoFiles - 1))];
		tv.src = '/video?file=' + video;
		tv.load();
		tv.play();
	}

	$('#fs').on('click', makeFS);
	function makeFS() {
		if (
			document.fullscreenEnabled ||
			document.webkitFullscreenEnabled ||
			document.mozFullScreenEnabled ||
			document.msFullscreenEnabled
		) {
			if (tv.requestFullscreen) {
				tv.requestFullscreen();
			} else if (tv.webkitRequestFullscreen) {
				tv.webkitRequestFullscreen();
			} else if (tv.mozRequestFullScreen) {
				tv.mozRequestFullScreen();
			} else if (tv.msRequestFullscreen) {
				tv.msRequestFullscreen();
			}
		}
	}

}());
