'use strict';

function toTitleCase(str) {
	return str
		.replace(/\-/g, ' ')
		.replace(/(^[a-z]|\s[a-z])/g, function (firstLetterOfWord) {
			return firstLetterOfWord.toUpperCase();
		});
}

function parseTitleFromFileName(fileName) {
	return fileName.slice(fileName.lastIndexOf('/') + 1, fileName.lastIndexOf('.')).replace(/_/g, ' ');
}

module.exports = {
	toTitleCase: toTitleCase,
	parseTitleFromFileName: parseTitleFromFileName
};